#include <QtGui>
#include <iostream>

#include "domaindialog.h"
#include "xmlreader.h"

XmlStreamReader::XmlStreamReader(slavesTree *tree)
{
    treeWidget = tree;
}

bool XmlStreamReader::readFile(const QString &fileName)
{
    QFile file(fileName);
    if (!file.open(QFile::ReadOnly | QFile::Text)) {
        std::cerr << "Error: Cannot read file " << qPrintable(fileName)
                  << ": " << qPrintable(file.errorString())
                  << std::endl;
        return false;
    }

    reader.setDevice(&file);

    reader.readNext();
    while (!reader.atEnd()) {
        if (reader.isStartElement()) {
            if (reader.name() == "EtherCATInfoList") {
                readEcatInfoListElement();
            } else {
                reader.raiseError(QObject::tr("Not a "));
            }
        } else {
            reader.readNext();
        }
    }

    file.close();
    if (reader.hasError()) {
        std::cerr << "Error: Failed to parse file "
                  << qPrintable(fileName) << ": "
                  << qPrintable(reader.errorString()) << std::endl;
        return false;
    } else if (file.error() != QFile::NoError) {
        std::cerr << "Error: Cannot read file " << qPrintable(fileName)
                  << ": " << qPrintable(file.errorString())
                  << std::endl;
        return false;
    }
    return true;
}

void XmlStreamReader::readEcatInfoListElement()
{
    QTreeWidgetItem *item = new QTreeWidgetItem(0);
    item->setText(0, "EtherCATInfoList");
    int slave_id = 0 ;

    reader.readNext();
    while (!reader.atEnd()) {
        if (reader.isEndElement()) {
            reader.readNext();
            break;
        }

        if (reader.isStartElement()) {
            if (reader.name() == "EtherCATInfo") {
                readEcatInfoElement(slave_id++);
            }else{
                skipUnknownElement();
            }
        }  else{
            reader.readNext();
        }
    }
}

void XmlStreamReader::readEcatInfoElement(int slave_id)
{
    QString vid;
    QTreeWidgetItem *vendorIdItem = 0 ;
    QTreeWidgetItem * device = 0;

    reader.readNext();
    while (!reader.atEnd()) {
        if (reader.isEndElement()) {
            reader.readNext();
            break;
        }


        if (reader.isStartElement()) {
            if (reader.name() == "Descriptions") {
                device = readDescriptionsElement(slave_id);
                continue;
            }

            if (reader.name() == "Vendor") {
                vendorIdItem = readVendorIdElement(vid);
                continue;
            }
            skipUnknownElement();
        } else{
            reader.readNext();
        }
    }

    if (device && vendorIdItem){
        device->addChild(vendorIdItem);
    }
    ec_ioctl_slave_t* sl;
    sl = treeWidget->Dialog()->master->getSlave(slave_id);
    sl->vendor_id = vid.toInt();
}

QTreeWidgetItem* XmlStreamReader::readVendorIdElement(QString &vid)
{
    QTreeWidgetItem *vidItem = NULL;
    QTreeWidgetItem *vendorElement = 0;

    reader.readNext();
    while (!reader.atEnd()) {

        if (reader.isEndElement()) {
            reader.readNext();
            break;
        }

        if (reader.isStartElement()) {
            if (reader.name() == "Id") {
                vendorElement =  new QTreeWidgetItem();
                vendorElement->setText(0, "Vendor");
                vidItem = readElement(vendorElement, "Id", NULL);
            }else{
                skipUnknownElement();
            }
        } else{
            reader.readNext();
        }
    }
    vid = vidItem->text(1);
    return vendorElement;
}

QTreeWidgetItem* XmlStreamReader::readDescriptionsElement(int slave_id)
{
    QTreeWidgetItem* device = 0;

    reader.readNext();
    while (!reader.atEnd()) {
        if (reader.isEndElement()) {
            reader.readNext();
            break;
        }

        if (reader.isStartElement()) {
            if (reader.name() == "Devices") {
                device = readDevicesElement(slave_id);
            }else{
                skipUnknownElement();
            }
        } else{
            reader.readNext();
        }
    }
    return device;
}

QTreeWidgetItem * XmlStreamReader::readDevicesElement(int slave_id)
{
    QTreeWidgetItem *device=NULL;

    reader.readNext();
    while (!reader.atEnd()) {
        QString name = reader.name().toString();
        if (reader.isEndElement()) {
            reader.readNext();
            break;
        }

        if (reader.isStartElement()) {
            if (reader.name() == "Device") {
                device = readDeviceElement(slave_id);
            }else{
                skipUnknownElement();
            }
        } else{
            reader.readNext();
        }
    }
    return device;
}

QTreeWidgetItem * XmlStreamReader::readDeviceElement(int slave_id)
{
    QTreeWidgetItem *item = new QTreeWidgetItem(treeWidget->invisibleRootItem());
    item->setText(0, "Device");
    item->setText(1,QString('0' + slave_id));
    ec_ioctl_slave_t* sl;

    sl = treeWidget->Dialog()->master->addSlave(slave_id);

    reader.readNext();
    while (!reader.atEnd()) {

        if (reader.isEndElement()) {
            reader.readNext();
            break;
        }

        if (reader.isStartElement()) {
            if (reader.name() == "Type") {
                 readTypeElement(item, slave_id);
                 continue;
            }

            if (reader.name() == "Name") {
                readElement(item, "Name", NULL);
                continue;
            }

            if (reader.name() == "Sm"){
                readSmElement(item, slave_id, sl->sync_count);
                sl->sync_count++;
                continue;
            }

            if (reader.name() == "RxPdo"){
                readPdoElement(slave_id, "RxPdo");
                continue;
            }

            if (reader.name() == "TxPdo"){
                readPdoElement(slave_id, "TxPdo");
                continue;
            }
            skipUnknownElement();

        } else{
            reader.readNext();
        }
    }
    return item;
}

void XmlStreamReader::readSmElement(QTreeWidgetItem *parent,int slavePos, int syncPos)
{
    QTreeWidgetItem *item = new QTreeWidgetItem(parent);

    item->setText(0, "SyncManager");
    item->setText(1, QString('0' + syncPos) );
    item->setText(2, reader.attributes().value("DefaultSize").toString());

    treeWidget->Dialog()->master->addSync(slavePos, syncPos);

    while(!reader.atEnd()){
        if (reader.isEndElement())
            break;
        reader.readNext();
    }
    reader.readNext();

}

void XmlStreamReader::readTypeElement(QTreeWidgetItem *parent,int slave_id)
{
    QTreeWidgetItem *item = new QTreeWidgetItem(parent);

    ec_ioctl_slave_t* slave =  treeWidget->Dialog()->master->getSlave(slave_id);

    item->setText(0, "ProductCode");
    QString productCode = reader.attributes().value("ProductCode").toString();
    productCode.replace('#','0');
    item->setText(1,productCode );
    slave->product_code = toHex(productCode.toStdString());

    item = new QTreeWidgetItem(parent);

    item->setText(0, "RevisionNo");
    QString revisionNr = reader.attributes().value("RevisionNo").toString();
    revisionNr.replace('#','0');
    item->setText(1, revisionNr);
    slave->revision_number = toHex(revisionNr.toStdString());

    while(!reader.atEnd()){
        if (reader.isEndElement())
            break;
        reader.readNext();
    }
    reader.readNext();

}

void XmlStreamReader::readPdoElement(int slavePos, QString name)
{
    QTreeWidgetItem *parent = NULL;
    QString pdoSyncManager = reader.attributes().value("Sm").toString();

    for (int i = 0; i < treeWidget->topLevelItemCount(); ++i) {
        QTreeWidgetItem *Device = treeWidget->topLevelItem(i);
        for (int j = 0; j < Device->childCount(); ++j) {
                QTreeWidgetItem *element = Device->child(j);
                if (element->text(0) != QString("SyncManager"))
                    continue;
                QString syncManagerID = element->text(1);
                if (syncManagerID == pdoSyncManager) {
                    parent = element;
                    break;
                }
        }
    }
    if (!parent) {
        std::cout << "Failed to find sync manager for pdo "
                  << name.toStdString() << " SyncManager id="
                  << pdoSyncManager.toStdString() << "\n";
        exit(-1);
    }

    QTreeWidgetItem *item = new QTreeWidgetItem(parent);

    item->setText(0, name);
    item->setText(1, reader.attributes().value("Sm").toString());
    item->setText(2, reader.attributes().value("Fixed").toString());

    MPdo *mpdo = treeWidget->Dialog()->master->addPdo(
                slavePos, pdoSyncManager.toInt() );

    MSyncManager *msync = treeWidget->Dialog()->master->Sync(
                slavePos,
                pdoSyncManager.toInt());

    EC_WRITE_BIT(&msync->sync.control_register, 2, 0);
    if ("RxPdo" == name) {
        EC_WRITE_BIT(&msync->sync.control_register, 2, 1);
    }
    reader.readNext();

    while (!reader.atEnd()) {

        if (reader.isEndElement()) {
            reader.readNext();
            break;
        }
        if (reader.isStartElement()) {

            if (reader.name() == "Index") {
                readElement(item,"Index", mpdo);
                continue;
            }

            if (reader.name() == "Name") {
                readElement(item, "Name" ,mpdo );
                continue;
            }

            if (reader.name() == "Entry") {
                readEntryElement(item , mpdo);
                continue;
            }
            skipUnknownElement();
        } else{
            reader.readNext();
        }
    }
}

void XmlStreamReader::readEntryElement(QTreeWidgetItem *parent, MPdo *mpdo)
{
    QTreeWidgetItem *item = new QTreeWidgetItem(parent);

    QString name = reader.name().toString();
    item->setText(0, "Entry");

    reader.readNext();

    while (!reader.atEnd()) {

        if (reader.isEndElement()) {
            reader.readNext();
            break;
        }

        if (reader.isStartElement()) {
            if (reader.name() == "Index") {
                readEntryElementIndex(item);
                continue;
            }
            if (reader.name() == "Name") {
                readElement(item, "Name", NULL);
                continue;
            }
            if (reader.name() == "BitLen") {
                readElement(item, "BitLen", NULL);
                continue;
            }
            if (reader.name() == "SubIndex") {
                readElement(item, "SubIndex", NULL);
                continue;
            }
            if (reader.name() == "DataType") {
                readElement(item, "DataType", NULL);
                continue;
            }
            skipUnknownElement();
          } else{
            reader.readNext();
        }
    }
    /*
     * we record all pdo entries from all slaves
     * in a single list.
    */
    QString Index;
    QString subIndex;
    QTreeWidgetItem *t;

    QTreeWidgetItem* itemIndex= treeWidget->getChild(item,
                           "Index", Index);
    QTreeWidgetItem* itemSubIndex=  treeWidget->getChild(item,
                            "SubIndex", subIndex);

    t = parent;
    QString pdoIndex;

    treeWidget->getChild(t, "Index", pdoIndex);

    QString SM = parent->parent()->text(1);
    QString slave = parent->parent()->parent()->text(1);
    /* add to entries list */
    pdoEntry entry(Index,
                   subIndex,
                   pdoIndex, SM, slave);
    entry._itemIndex =  itemIndex;
    entry._itemSubIndex =  itemSubIndex;
    entry._itemEntry = item;
    entries.push_back(entry);

    /* add to Master tree */
    QString bitLen;
    treeWidget->getChild(item,  "BitLen", bitLen);
    QString entryName;
    treeWidget->getChild(item,  "Name", entryName);
    treeWidget->Dialog()->master->addEntry(mpdo,
                                           toHex(Index.toStdString()),
                                           toHex(subIndex.toStdString()),
                                           bitLen.toInt(),
                                           entryName.toStdString());

}

/***************************************************************************************/

QTreeWidgetItem* XmlStreamReader::readElement(QTreeWidgetItem *parent,QString element,MPdo *mpdo)
{
    QTreeWidgetItem *item = new QTreeWidgetItem(parent);
    QString text = reader.readElementText();

    while(!reader.atEnd()){
        if (reader.isEndElement())
            break;
        reader.readNext();
    }
    reader.readNext();

    item->setText(0, element);
    item->setText(1, text);
    if (element == "Index" ) {
        int s = text.size();
        QChar c;
        /* remove any none letter or number */
        for (int i = 0; i  < s ; i++){
            c = text.at(i);
            if (c.isDigit())
                continue;
            if (c.isLetter())
                continue;
            if (c == '#') {
                text.replace(i,1,'0');
               continue;
            }
            text.remove(i,1);
        }
        item->setText(1, text);

        if (mpdo) {
            mpdo->pdo.index = toHex(text.toStdString());
       }
    }
    return item;
}

void XmlStreamReader::readEntryElementIndex(QTreeWidgetItem *parent)
{
    QTreeWidgetItem *item = new QTreeWidgetItem(parent);
    QString text = reader.readElementText();

    while(!reader.atEnd()){
        if (reader.isEndElement())
            break;
        reader.readNext();
    }
    reader.readNext();

    item->setText(0, "Index");
    int s = text.size();

    QChar c;
    /* remove any none letter or number */
    for (int i = 0; i  < s ; i++){
            c = text.at(i);
            if (c.isDigit())
                continue;
            if (c.isLetter())
                continue;
            if (c == '#') {
                text.replace(i, 1, '0');
               continue;
            }
            text.remove(i,1);
    }
    item->setText(1, text);
}


void XmlStreamReader::update_pdoEntryList(
        QTreeWidgetItem *itemIndex,
        QTreeWidgetItem *itemSubIndex,
        QString& Index,
        QString &subIndex,
        QString& pdo,
        QString &sm,
        QString &slave)
{
    std::list<pdoEntry>::iterator iter  = entries.begin();

    for (;  iter != entries.end() ;  iter++) {
        pdoEntry*e = &(*iter);

        if (e->_Index == Index
            && e->_subIndex == subIndex
            && e->_pdo == pdo
            &&  e->_sm == sm
            &&  e->_slave == slave ) {

            e->_itemIndex = itemIndex;
            e->_itemSubIndex = itemSubIndex;
        }
    }
}

void XmlStreamReader::skipUnknownElement()
{
    reader.readNext();
    while (!reader.atEnd()) {
        if (reader.isEndElement()) {
            reader.readNext();
            break;
        }

        if (reader.isStartElement()) {
            skipUnknownElement();
        } else {
            reader.readNext();
        }
    }
}
