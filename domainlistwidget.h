#ifndef PROJECTLISTWIDGET_H
#define PROJECTLISTWIDGET_H

#include <QListWidget>
#include <QTreeWidgetItem>
#include "pdodomainitem.h"

class ProjectDialog;

class domainListWidget : public QListWidget
{
    Q_OBJECT

public:
    domainListWidget(QWidget *parent = 0);
    void setDialog(ProjectDialog *d) {_dialog=d;}    
    void processDomain();

protected:

    void dropEvent(QDropEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void dragEnterEvent(QDragEnterEvent *event);
    void dragMoveEvent(QDragMoveEvent *event);

private:

    void performDrag();
    QPoint startPos;
    ProjectDialog *_dialog;
};

#endif
