QT           += core gui widgets
TARGET        = domaindialog
TEMPLATE      = app
HEADERS       = domaindialog.h \
                domainlistwidget.h \
                slavewidget.h \
                MainWindow.h \
   		xmlreader.h \
    icodegenerator.h \
    codecgenerator.h \
    pdoEntry.h \
    pdodomainitem.h \
    masterdevice.h
SOURCES       = main.cpp \
                domaindialog.cpp \
                domainlistwidget.cpp \
    		slavewidget.cpp \
   		xmlreader.cpp \
    		MainWindow.cpp \
    codecgenerator.cpp \
    pdodomainitem.cpp \
    masterdevice.cpp \
    CommandCStruct.cpp
FORMS         = domaindialog.ui
RESOURCES     = domainchooser.qrc
