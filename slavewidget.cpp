#include "slavewidget.h"
#include "domainlistwidget.h"
#include <iostream>
#include "domaindialog.h"
#include "xmlreader.h"

slavesTree::slavesTree(QWidget *parent)
    : QTreeWidget(parent)
{

}

/*
 * when droppping back into the slave tree
 */
bool slavesTree::dropMimeData(QTreeWidgetItem *parent, int index __attribute ((__unused__)),
                               const QMimeData *data, Qt::DropAction action __attribute ((__unused__)))
{
	QList<QUrl> urlList;

    if (!parent){
        return false;
    }

    if (!parent->parent())
        return false;

    if (!data->hasText() ){
        return false;
    }

    if (parent->text(0) != "Entry") {
        if ( parent->parent()->text(0) != "Entry" )
            return false;
        parent = parent->parent();
    }

    QString entryIndex(data->text());

    int l = entryIndex.indexOf("#",0);
    QString sourceFullPath = entryIndex.mid(l+1);
    QString destFullPath;

    getEntryFullPath(parent, destFullPath);
    if (destFullPath != sourceFullPath)
        return false;

    entryIndex.chop(entryIndex.size() - l);
    int column_pos = entryIndex.indexOf(':',0);

    QString entrySubIndex = entryIndex.mid(column_pos + 1);
    entryIndex.chop(entryIndex.size() - column_pos);

    pdoItem *itemIndex = new pdoItem(parent);
    itemIndex->setText(0, "Index");
    itemIndex->setText(1, entryIndex);

    QTreeWidgetItem* itemSubIndex = new QTreeWidgetItem(parent);
    itemSubIndex->setText(0,"SubIndex");
    itemSubIndex->setText(1, entrySubIndex);

    QString pdo;
    QTreeWidgetItem*  PDO = getChild(parent->parent(), "Index", pdo);
    QString sm =  PDO->parent()->text(1);
    QString slave = PDO->parent()->parent()->parent()->text(1);

    std::list<pdoEntry>::iterator iter  = _dialog->xmlFile->entries.begin();

    for (;  iter != _dialog->xmlFile->entries.end() ;  iter++) {

        pdoEntry *e = &(*iter);


        if ( !(e->_Index == entryIndex   && e->_subIndex == entrySubIndex
                && e->_pdo == pdo  &&  e->_sm == sm) )
                continue;

        if (slave == e->_slave) {
            e->_itemIndex = itemIndex;
            e->_itemSubIndex = itemSubIndex;
            continue;
        }

        if (e->_itemIndex == NULL){
            pdoItem *itemIndex2 = new pdoItem(e->_itemEntry);
            itemIndex2->setText(0, "Index");
            itemIndex2->setText(1, entryIndex);

            QTreeWidgetItem* itemSubIndex2 = new QTreeWidgetItem(e->_itemEntry);
            itemSubIndex2->setText(0,"SubIndex");
            itemSubIndex2->setText(1, entrySubIndex);

            e->_itemIndex = itemIndex2;
            e->_itemSubIndex = itemSubIndex2;
        }
    }
    return true;
}

bool slavesTree::dropMimeData(domainListWidget *parent __attribute__ ((unused)),
                              int index __attribute__ ((unused)),
                              const QMimeData *data,
                              Qt::DropAction action __attribute__ ((unused)))
{
    QList<QUrl> urlList;
    QTreeWidgetItem *item;

    urlList = data->urls(); // retrieve list of urls

    foreach(QUrl url, urlList) // iterate over list
    {
        // make new QTreeWidgetItem and set its text
        // if parent is null - add top level item (this parent)
       item = new QTreeWidgetItem(this);
        // set item text.
        // we always drop indexes
        item->setText( 0, url.toLocalFile() );
    }
    return true;
}

QStringList slavesTree::mimeTypes () const
{
	QStringList qstrList;
    // list of accepted mime types for drop
	qstrList.append("text/uri-list");
	return qstrList;
}


Qt::DropActions slavesTree::supportedDropActions () const
{
	// returns what actions are supported when dropping
    return Qt::MoveAction;
}

void slavesTree::performDrag()
{
    QTreeWidgetItem *item = currentItem();
    if (!item)
        return;

    if (item->text(0) != "Index")
        return;

    QTreeWidgetItem *parent = item->parent();
    if (parent->text(0) != "Entry" )
        return;

    QMimeData *mimeData = new QMimeData;
    QList<QUrl> list;

    QTreeWidgetItem *pdoIndex = currentItem();
    /* scan for subindex */
    QTreeWidgetItem *pdoSubIndex = NULL;
    for (int i = 0 ; i < parent->childCount(); i++){
            QTreeWidgetItem *subIndex = parent->child(i);
            if (subIndex->text(0) == "SubIndex"){
                pdoSubIndex = subIndex;
                break;
            }
    }
    if (pdoSubIndex == NULL){
        std::cerr << "Invalid XML. no subindex for "
                  << pdoIndex->text(1).toStdString() << std::endl;
        return;
    }
    QString pdoSlave(pdoIndex->text(1)  + QString(":")
                     + pdoSubIndex->text(1) );

    pdoSlave.append("#");

    /*
     * The pdo entry full path is saved in the 3-rd column
     */
    QString entryfullPath;
    getEntryFullPath(pdoIndex->parent(), entryfullPath);
    pdoSlave.append(entryfullPath);

    mimeData->setText(pdoSlave);
    list.append(QUrl( pdoSlave )); // only QUrl in list will be text of actual item

    // mime stuff
    mimeData->setUrls(list);

    QDrag *drag = new QDrag(this);
    drag->setMimeData(mimeData);
    drag->setPixmap(QPixmap(":/images/person.png"));
    if (drag->exec(Qt::MoveAction) == Qt::MoveAction){

        QTreeWidgetItem *PDO = parent->parent();
        QString pdo;
        getChild(PDO, "Index", pdo);

        QString syncIndex = PDO->text(1);
        QTreeWidgetItem* SM = PDO->parent();
        QTreeWidgetItem* Device = SM->parent();
        QString slave = Device->text(1);
        QString Index = pdoIndex->text(1);
        QString subIndex = pdoSubIndex->text(1);

        _dialog->xmlFile->update_pdoEntryList(
                    NULL,
                    NULL,
                    Index,
                    subIndex,
                    pdo,
                    syncIndex,
                    slave );

        delete pdoIndex;
        delete pdoSubIndex;

    }

}

void slavesTree::mouseMoveEvent(QMouseEvent *event)
{
 //   printf("slavesTree: %s \n",__func__);
    // if not left button - return
    if (!(event->buttons() & Qt::LeftButton))
        return;
    performDrag();
}

void slavesTree::dragEnterEvent(QDragEnterEvent *event)
{
//    printf("slavesTree: %s \n",__func__);
    domainListWidget *source =
            qobject_cast<domainListWidget *>(event->source());
    if (source) {
        event->setDropAction(Qt::MoveAction);
        event->accept();
    }
}

void slavesTree::dragMoveEvent(QDragMoveEvent *event)
{
  //  printf("slavesTree: %s \n",__func__);
    domainListWidget *source =
            qobject_cast<domainListWidget *>(event->source());
    if (source) {
        event->setDropAction(Qt::MoveAction);
        event->accept();
    }
}


/* get Entry the Full Path up to slave id */
bool slavesTree::getEntryFullPath(QTreeWidgetItem *Entry,QString& fullPath)
{
    QTreeWidgetItem *PDO = Entry->parent();
    QString PdoIndex;

    if (!getChild(PDO,
                  "Index", PdoIndex)){
        return false;
    }

    QString syncIndex = PDO->text(1);

    fullPath.append(PdoIndex);
    fullPath.append("-");
    fullPath.append(syncIndex);
    fullPath.append("-");

    QTreeWidgetItem* syncM = PDO->parent();
    QTreeWidgetItem* Device = syncM->parent();

    fullPath.append(Device->text(1));
    return true;
}

/*
 *scan all childrens and fetch value on second column
*/
QTreeWidgetItem *slavesTree::getChild(QTreeWidgetItem *root,
              QString variable,QString& value)
{
     for (int j = 0; j < root->childCount(); ++j) {
            QTreeWidgetItem *entry = root->child(j);
            if (variable == entry->text(0)) {
                value = QString(entry->text(1));
                return entry;
            }
     }
     return NULL;
}
