#include <QtGui>
#include <iostream>
#include <string>

#include "domainlistwidget.h"
#include "slavewidget.h"
#include "domaindialog.h"
#include "pdoEntry.h"
#include "xmlreader.h"

domainListWidget::domainListWidget(QWidget *parent)
    : QListWidget(parent)
{
    setAcceptDrops(true);
}


void domainListWidget::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
        startPos = event->pos();
    QListWidget::mousePressEvent(event);
}

void domainListWidget::mouseMoveEvent(QMouseEvent *event)
{
    if (event->buttons() & Qt::LeftButton) {
        int distance = (event->pos() - startPos).manhattanLength();
        if (distance >= QApplication::startDragDistance())
            performDrag();
    }
    QListWidget::mouseMoveEvent(event);
}

void domainListWidget::dragEnterEvent(QDragEnterEvent *event)
{
    event->setDropAction(Qt::MoveAction);
    event->accept();
}

void domainListWidget::dragMoveEvent(QDragMoveEvent *event)
{
    domainListWidget *source =
            qobject_cast<domainListWidget *>(event->source());

    if (source && source != this) {
        event->setDropAction(Qt::MoveAction);
        event->accept();
    }
}

void domainListWidget::dropEvent(QDropEvent *event)
{

    slavesTree *src = qobject_cast<slavesTree *>(event->source());
    domainListWidget *source =
            qobject_cast<domainListWidget *>(event->source());

    if (event->mimeData()->isWidgetType()) {
        return;
    }

    if (!(src || (source && source != this)))
        return;

    QString pdoString(event->mimeData()->text());

    pdoDomainItem *Item  = new pdoDomainItem();

    int l = pdoString.indexOf("#", 0);

    QString parentPath = pdoString.mid(l+1);

    pdoString.chop(pdoString.size() - l);
    Item->setText(pdoString);

    Item->setEntry(pdoString);
    Item->setParentPath(parentPath);
    Item->setToolTip(parentPath);

    addItem(Item);

    event->setDropAction(Qt::MoveAction);
    event->accept();
    _dialog->mayGenerateCode(true);
}

void domainListWidget::performDrag()
{
    QListWidgetItem *item = currentItem();
    pdoDomainItem *m  = (pdoDomainItem *)item;

    if (!item)
        return;

    QMimeData *mimeData = new QMimeData;

    QString itemFullName(item->text());
    itemFullName.append("#");
    itemFullName.append(m->getParentPath());
    mimeData->setText(itemFullName);

    QDrag *drag = new QDrag(this);
    drag->setMimeData(mimeData);
    drag->setPixmap(QPixmap(":/images/person.png"));

    if (drag->exec(Qt::MoveAction)
                == Qt::MoveAction){
            delete item;
    }

    if ( this->count() == 0 )
        _dialog->mayGenerateCode(false);

}

void domainListWidget::processDomain()
{
    QString domainName  = objectName();

    _dialog->codeGenerator->createDomain(domainName);

    int count = this->count();

    for (int i = 0 ; i < count ; i++) {

        pdoDomainItem *entryItem = (pdoDomainItem *)item(i);

        QString pdoIndex(entryItem->text());

        int column_pos = pdoIndex.indexOf(':',0);

        QString pdoSubIndex = pdoIndex.mid(column_pos + 1);
        pdoIndex.chop(pdoIndex.size() - column_pos);

        QString entryIndex, entrySubIndex, Index, SM, slave;

        entryItem->getElements(entryIndex,
                               entrySubIndex,
                               Index,
                               SM, slave);

        QString Domain(objectName());

        QString Device = QString("Device") + slave;

        _dialog->codeGenerator->addPdoEntry(Device,
                                            pdoIndex,
                                            pdoSubIndex, Domain);

        std::list<pdoEntry>::iterator iter = _dialog->xmlFile->entries.begin();

        for ( ;iter != _dialog->xmlFile->entries.end() ; iter++) {
            pdoEntry *e = &(*iter);

            if (e->_itemIndex)
                continue;
            QString nextDevice = QString("Device") + e->_slave;

            if (nextDevice == Device)
                continue;
            if (e->_Index != pdoIndex)
                continue;
            if (e->_subIndex != pdoSubIndex)
                continue;
            _dialog->codeGenerator->addPdoEntry(nextDevice,
                                                e->_Index,
                                                e->_subIndex, Domain);

         }
    }
}
