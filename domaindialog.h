#ifndef PROJECTDIALOG_H
#define PROJECTDIALOG_H

#include <QDialog>

#include "ui_domaindialog.h"
#include "icodegenerator.h"
#include "masterdevice.h"

class XmlStreamReader;
class MainWindow;

class ProjectDialog : public QDialog, private Ui::ProjectDialog
{
    Q_OBJECT

public:

    ProjectDialog(QWidget *parent=NULL);

    void setMainWindow(MainWindow *m) { _mainWnd = m;}
    void newDomain();
    void openFile(QString *file);
    void markSharedPdos();
    void openXMLFile(QString  file);
    void mayGenerateCode(bool state);
    void appendText(QString &text);
    bool writeCFile(const QString &filename);

    ICodeGenerator *codeGenerator;
    XmlStreamReader* xmlFile;
    MasterDevice *master;

private slots:

    void collectEntriesAction();
    void generateCode();

private:
    domainListWidget* getActiveDomain();
    void getActiveDomainsList(QStringList &);
    void getActiveSlavesList(QStringList &);
    void ResetDomain(domainListWidget *domain,int index);
    void processItem(QTreeWidgetItem *parent);
    void compileProgram();
private:

    QAction *collectAction;
    MainWindow* _mainWnd;
};

#endif
