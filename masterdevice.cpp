
#include "masterdevice.h"
#include <sys/types.h>
#include <string.h>
#include <iostream>

MasterDevice::MasterDevice(ProjectDialog *d)
{
    _dialog = d;
}

/***********************************************************************/

ec_ioctl_slave_t* MasterDevice::addSlave(int& slavePos)
{
    MSlave sl;

    memset(&sl.slave,0,sizeof(sl.slave));
    sl.slave.position = slavePos;
    slaves.push_back(sl);
    return getSlave(slavePos);
}

/***********************************************************************/

ec_ioctl_slave_t*  MasterDevice::getSlave(int pos)
{
    MSlave* sl = Slave(pos);
    if (sl)
        return &sl->slave;
    return NULL;
}

/***********************************************************************/

MSlave* MasterDevice::Slave(int slavePos)
{
    std::list<MSlave>::iterator iter  = slaves.begin();

    for (; iter != slaves.end() ;  iter++) {

        MSlave *sl = &(*iter);
        if (sl->slave.position == slavePos)
                return sl;
    }
    return NULL;
}

/***********************************************************************/

void MasterDevice::addSync(int SlavePos,int syncPos)
{
    MSlave *slave = Slave(SlavePos);
    if(!slave)
        return;

    MSyncManager SM;
    memset(&SM.sync,0,sizeof(SM.sync));

    SM.sync.sync_index = syncPos;

    slave->syncs.push_back(SM);
}

/***********************************************************************/

MSyncManager* MasterDevice::Sync(int slavePos,unsigned int syncPos)
{
    MSlave* slave = Slave(slavePos);

    std::list<MSyncManager>::iterator iter  =  slave->syncs.begin();

    for (; iter != slave->syncs.end() ;  iter++) {

        MSyncManager *sync = &(*iter);
        if (syncPos-- == 0)
            return sync;
    }
    throw "Failed to find sync manager";
    return NULL;
}

/***********************************************************************/

void MasterDevice::getSync(ec_ioctl_slave_sync_t* sync, int slavePos,unsigned int syncPos)
{
    MSyncManager *MSyncM =  Sync(slavePos, syncPos);
    *sync = MSyncM->sync;
}

/***********************************************************************/

MPdo* MasterDevice::addPdo(int slavePos,int syncPos)
{
   MSyncManager *MSyncM =  Sync(slavePos, syncPos);
   MPdo mpdo;

   memset(&mpdo.pdo,0,sizeof(mpdo.pdo));
   mpdo.pdo.entry_count = 0;
   mpdo.pdo.pdo_pos =  MSyncM->pdos.size();
   MSyncM->pdos.push_back(mpdo);
   MSyncM->sync.pdo_count++;
   return mPdo(slavePos, syncPos,mpdo.pdo.pdo_pos);
}

/***********************************************************************/

MPdo* MasterDevice::mPdo(int slavePos, int syncPos, int pdoPos)
{
    MSyncManager *MSyncM  =  Sync(slavePos, syncPos);
    std::list<MPdo>::iterator iter  =  MSyncM->pdos.begin();
    MPdo *mpdo = NULL;

    for (; iter != MSyncM->pdos.end() ;  iter++) {
        mpdo = &(*iter);
        if (pdoPos-- == 0){
           return mpdo;
        }
    }
    throw "Failed to find PDO";
    return NULL;
}

/***********************************************************************/

void MasterDevice::getPdo( ec_ioctl_slave_sync_pdo_t *pdo, int slavePos, int syncPos, int pdoPos)
{
    MPdo *mpdo = mPdo(slavePos, syncPos, pdoPos);
    if (mpdo){
        memcpy(pdo, &mpdo->pdo,sizeof(*pdo));
    }else{
        throw "Not Pdo found";
    }
}

/***********************************************************************/
void MasterDevice::addEntry(MPdo *mpdo,
                            int index,
                            int subIndex,
                            int bitLen,
                            std::string entryName)

{
    MPdoEntry pdoE;

    pdoE.entry.index = index;
    pdoE.entry.subindex = subIndex;
    pdoE.entry.bit_length = bitLen;
    mpdo->pdo.entry_count++;
    strncpy((char *)pdoE.entry.name, (char *)entryName.c_str(),
            sizeof(pdoE.entry.name));

    mpdo->entries.push_back(pdoE);
}

/***********************************************************************/

void  MasterDevice::getPdoEntry(ec_ioctl_slave_sync_pdo_entry_t* entry,
                                int slavePos,
                                int syncPos,
                                int pdoPos,
                                int entryPos)
{
    MPdo *mpdo = mPdo(slavePos, syncPos, pdoPos);

    std::list<MPdoEntry>::iterator iter =
            mpdo->entries.begin();
    MPdoEntry *pdoE = NULL;

     for (; iter != mpdo->entries.end() ;  iter++) {
         pdoE = &(*iter);
         if (entryPos-- == 0){            
             *entry = pdoE->entry;
             return;
         }
     }
     throw "get Pdo entry error";
}

/***********************************************************************/

void  MasterDevice::selectedSlaves(std::list<MSlave> &selectedSlaves)
{
    selectedSlaves = slaves;
}
