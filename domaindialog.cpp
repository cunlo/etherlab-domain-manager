//#include <QtGui>
#include <QStatusBar>
#include <string>
#include <iostream>
#include <stdio.h>

#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <sstream>
#include <iomanip>
#include <string.h>
#include <unistd.h>

#include "MainWindow.h"

using namespace std;

#include "domaindialog.h"
#include "slavewidget.h"
#include "xmlreader.h"
#include "codecgenerator.h"

void ProjectDialog::ResetDomain(domainListWidget *d,int i)
{
    d->hide();

    QString tooltip = QString("Domain ") + QString( toascii('0' + i) );
    d->setToolTip(tooltip);
    d->setDialog(this);
}

ProjectDialog::ProjectDialog(QWidget *parent)
    : QDialog(parent)
{
    setupUi(this);

    ResetDomain(domain, 1);
    domain->show();
    ResetDomain(domain_2, 2);
    ResetDomain(domain_3, 3);
    ResetDomain(domain_4, 4);
    ResetDomain(domain_5, 5);
    ResetDomain(domain_6, 6);
    ResetDomain(domain_7, 7);

    _slavesList->setHeaderLabels(
            QStringList() << tr("Name") << tr("Position"));

    _slavesList->setAcceptDrops(true);
    _slavesList->setProjectDialog(this);

    setWindowTitle(tr("Etherlab Domains Editor"));
    xmlFile =  new XmlStreamReader(_slavesList);

    codeGenerateButton->setEnabled(false);

    connect(codeGenerateButton,
            SIGNAL(clicked()),
            this, SLOT(generateCode()));

    collectAction = new QAction(tr("&Collect"), this);
    collectAction->setShortcut(QKeySequence::Cut);
    collectAction->setStatusTip(tr("Collect all identical"
                                   "entries from all slaves"));
    connect(collectAction, SIGNAL(triggered()),
            this, SLOT(collectEntriesAction()));
    addAction(collectAction);
    setContextMenuPolicy(Qt::ActionsContextMenu);

    codeGenerator = new codeCgenerator(this);
    master = new MasterDevice(this);
}

void ProjectDialog::collectEntriesAction()
{
    domainListWidget *d =  getActiveDomain();
    if (!d)
        return;

    pdoDomainItem *entry = (pdoDomainItem *) d->currentItem();

    QString Index;
    QString subIndex;
    QString pdo;
    QString sm;
    QString slave;
    entry->getElements(Index, subIndex, pdo, sm, slave);


    for (std::list<pdoEntry>::iterator iter
                        = xmlFile->entries.begin();
        iter != xmlFile->entries.end() ; iter++) {
        pdoEntry *e = &(*iter);


        if (e->_Index == Index && e->_subIndex == subIndex
                && e->_pdo == pdo &&  e->_sm == sm) {

                delete e->_itemIndex;
                delete e->_itemSubIndex;
                e->_itemIndex = 0;
                e->_itemSubIndex = 0;
            }
        }
}

void ProjectDialog::appendText(QString &text)
{
    plainTextEdit->appendPlainText(text);
}

void ProjectDialog::generateCode()
{
    plainTextEdit->clear();

    codeGenerator->createIncludes();

    codeGenerator->createStructs();

    /* we scan all domains and apply all rules */
    QStringList domains;
    getActiveDomainsList(domains);

    QStringList slavesList;
    getActiveSlavesList(slavesList);
    QString main("int ether_main()\n");

    codeGenerator->startMain(main,domains, slavesList);
    codeGenerator->requestMaster();

    /* generate create_slave */
    for ( int i = 0  ;  i  < _slavesList ->topLevelItemCount();    i++){
            QTreeWidgetItem *slave =  _slavesList->topLevelItem(i);
            QString slaveVar(slave->text(0));
            QString pos( slave->text(1) );
            slaveVar.append(pos);

            QString alias("0");
            QString vendorid;
            QString productCode;
            QTreeWidgetItem *Vendor;

            _slavesList->getChild(slave,"ProductCode", productCode);
            Vendor = _slavesList->getChild(slave, "Vendor",vendorid);
            _slavesList->getChild(Vendor,"Id",vendorid);

            codeGenerator->configureSlave(slaveVar,
                                          alias,
                                          pos,
                                          vendorid, productCode);
            codeGenerator->configurePDos(pos);
    }


    if (!domain->isHidden()) {
        domain->processDomain();
    }

    if (!domain_2->isHidden())
        domain_2->processDomain();

    if (!domain_3->isHidden())
        domain_3->processDomain();

    codeGenerator->requestMaster();


    foreach(const QString &str, domains){
        QString domain = str;
        codeGenerator->domainData(domain);
    }



    QString end_Main("}");
    codeGenerator->endMain(end_Main);
    compileProgram();
}

void ProjectDialog::compileProgram()
{
    stringstream cfile;
    QString libetest("libtectest.so");

    cfile << "_" << getpid() << ".c";

    QString tmpfile(cfile.str().c_str());
    bool ret = writeCFile(tmpfile);
    if (!ret){
        _mainWnd->statusBar()->showMessage(tr("Compile Error"), 5000);
        return;
    }

    int pid = fork();

    if (pid == 0 ) {
         stringstream outfile;

         outfile << "/tmp/compile." << getpid() << ".out";

         unlink(outfile.str().c_str());
         int fd = ::open(outfile.str().c_str(),
                       O_WRONLY|O_CREAT, 0666);
         ::dup2(fd, 1);
         ::dup2(fd, 2);

         ::close(fd);
         stringstream compile;
         compile << "/usr/bin/gcc -shared -fpic "
                 << cfile.str().c_str() << " -o "
                 << libetest.toStdString();

         const char* c = compile.str().c_str();

         const char* exec_args[4];
         exec_args[0] = "/bin/sh";
         exec_args[1] = "-c";
         exec_args[2] = c;
         exec_args[3] = 0x00;

         if ( ::execv("/bin/sh", (char *const *)exec_args) ){
             _mainWnd->statusBar()->showMessage(
                         tr( "Unknown compiler error" ), 5000);
             exit(-1);
         }
         exit(0);
     }
    QString compilerMsg = QString("compiling ") + libetest + QString(" ...");
    _mainWnd->statusBar()->showMessage(
                tr( compilerMsg.toStdString().c_str() ) , 1000);
    int status;
    ::waitpid(pid, &status, WUNTRACED);
    if ( WIFEXITED(status) < 0 ){
        _mainWnd->statusBar()->showMessage(tr("Compile Error."
                            "please compile manualy"), 5000);
    }
    unlink(tmpfile.toStdString().c_str());
}

void ProjectDialog::mayGenerateCode(bool state)
{
    if (state == false){
        codeGenerateButton->setEnabled(false);
        return;
    }
    codeGenerateButton->setEnabled(true);
}

void ProjectDialog::openXMLFile(QString filename)
{
    xmlFile->readFile(filename);
}

void ProjectDialog::newDomain()
{
    if (domain_2->isHidden()){
        domain_2->show();
        return;
    }
    if (domain_3->isHidden()){
        domain_3->show();
        return;
    }
    if (domain_4->isHidden()){
        domain_4->show();
        return;
    }
    if (domain_5->isHidden()){
        domain_5->show();
        return;
    }
    if (domain_6->isHidden()){
        domain_6->show();
        return;
    }
    if (domain_7->isHidden()){
        domain_7->show();
        return;
    }
}

void ProjectDialog::getActiveSlavesList(QStringList &slaveslist)
{

    for ( int i = 0  ;
          i  < _slavesList->topLevelItemCount();
          i++){
            QTreeWidgetItem *slave =  _slavesList->topLevelItem(i);
            QString slaveVar(slave->text(0));
            slaveVar.append(slave->text(1));
            slaveslist.push_back(slaveVar);
    }
}

void ProjectDialog::getActiveDomainsList(QStringList &domains)
{
    if (!domain->isHidden())
        domains.push_back( domain->objectName() );

    if (!domain_2->isHidden())
        domains.push_back( domain_2->objectName() );

    if (!domain_3->isHidden())
        domains.push_back( domain_3->objectName() );

    if (!domain_4->isHidden())
        domains.push_back( domain_4->objectName());

    if (!domain_5->isHidden())
        domains.push_back( domain_5->objectName() );

    if (!domain_6->isHidden())
        domains.push_back( domain_6->objectName() );

    if (!domain_7->isHidden())
        domains.push_back( domain_7->objectName() );
}

domainListWidget* ProjectDialog::getActiveDomain()
{
    if (domain->isActiveWindow())
            return domain;

    if (domain_2->isActiveWindow())
            return domain_2;

    if (domain_3->isActiveWindow())
            return domain_3;

    if (domain_4->isActiveWindow())
            return domain_4;

    if (domain_5->isActiveWindow())
            return domain_5;

    if (domain_6->isActiveWindow())
            return domain_6;

    if (domain_7->isActiveWindow())
            return domain_7;
    return NULL;
}

bool ProjectDialog::writeCFile(const QString &filename)
{
    QFile file(filename);
    file.open(QFile::WriteOnly | QFile::Text);

    file.write( plainTextEdit->toPlainText().toStdString().c_str());
    file.close();
    return true;
}
