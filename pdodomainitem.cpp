#include "pdodomainitem.h"

/* PDO-SM-SLAVE*/
void pdoDomainItem::setParentPath(QString &path)
{
    _parentPath = path;

    int dash1 = path.indexOf("-", 0);
    int dash2 = path.lastIndexOf("-");

    _SM = path.mid(dash1 + 1, 1);
    _slave = path.mid(dash2+1);
    _pdoIndex = path.mid(0, dash1);

}

/* INDEX:SUBINDEX */
void pdoDomainItem::setEntry(QString &entry)
{
    int colomn = entry.indexOf(":");
    _Index = entry.mid(0,colomn);
    _subIndex = entry.mid(colomn + 1);
}

void pdoDomainItem::getElements(QString &index,
                                QString &subIndex,
                                QString &pdoIndex,
                                QString &sm,QString &slave)
{
    index = _Index;
    subIndex = _subIndex;
    pdoIndex = _pdoIndex;
    sm = _SM;
    slave = _slave;
}
