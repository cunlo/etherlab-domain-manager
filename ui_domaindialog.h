/********************************************************************************
** Form generated from reading UI file 'domaindialog.ui'
**
** Created: Thu Feb 28 22:29:14 2013
**      by: Qt User Interface Compiler version 4.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DOMAINDIALOG_H
#define UI_DOMAINDIALOG_H

#include <QtCore/QVariant>
#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QDialog>
#include <QFrame>
#include <QHeaderView>
#include <QLabel>
#include <QPlainTextEdit>
#include <QPushButton>
#include <QTabWidget>
#include <QWidget>
#include "domainlistwidget.h"
#include "slavewidget.h"

QT_BEGIN_NAMESPACE

class Ui_ProjectDialog
{
public:
    QTabWidget *tabWidget;
    QWidget *tabObjectConf;
    slavesTree *_slavesList;
    domainListWidget *domain;
    QLabel *domainsLabel;
    QFrame *line;
    domainListWidget *domain_2;
    domainListWidget *domain_3;
    domainListWidget *domain_4;
    domainListWidget *domain_5;
    domainListWidget *domain_6;
    domainListWidget *domain_7;
    QPushButton *codeGenerateButton;
    QWidget *tabOutputConsole;
    QPlainTextEdit *plainTextEdit;

    void setupUi(QDialog *ProjectDialog)
    {
        if (ProjectDialog->objectName().isEmpty())
            ProjectDialog->setObjectName(QString::fromUtf8("ProjectDialog"));
        ProjectDialog->resize(753, 411);
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(1);
        sizePolicy.setVerticalStretch(1);
        sizePolicy.setHeightForWidth(ProjectDialog->sizePolicy().hasHeightForWidth());
        ProjectDialog->setSizePolicy(sizePolicy);
        ProjectDialog->setStyleSheet(QString::fromUtf8(""));
        ProjectDialog->setSizeGripEnabled(true);
        tabWidget = new QTabWidget(ProjectDialog);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tabWidget->setGeometry(QRect(10, 20, 721, 371));
        QSizePolicy sizePolicy1(QSizePolicy::Maximum, QSizePolicy::Maximum);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(tabWidget->sizePolicy().hasHeightForWidth());
        tabWidget->setSizePolicy(sizePolicy1);
        tabWidget->setMinimumSize(QSize(0, 0));
        QFont font;
        font.setFamily(QString::fromUtf8("Andale Mono"));
        font.setPointSize(10);
        tabWidget->setFont(font);
        tabWidget->setUsesScrollButtons(false);
        tabWidget->setDocumentMode(false);
        tabWidget->setTabsClosable(false);
        tabWidget->setMovable(true);
        tabObjectConf = new QWidget();
        tabObjectConf->setObjectName(QString::fromUtf8("tabObjectConf"));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(tabObjectConf->sizePolicy().hasHeightForWidth());
        tabObjectConf->setSizePolicy(sizePolicy2);
        _slavesList = new slavesTree(tabObjectConf);
        QTreeWidgetItem *__qtreewidgetitem = new QTreeWidgetItem();
        __qtreewidgetitem->setText(1, QString::fromUtf8("2"));
        __qtreewidgetitem->setText(0, QString::fromUtf8("1"));
        __qtreewidgetitem->setBackground(0, QColor(8, 13, 0));
        _slavesList->setHeaderItem(__qtreewidgetitem);
        _slavesList->setObjectName(QString::fromUtf8("_slavesList"));
        _slavesList->setGeometry(QRect(0, 10, 321, 271));
        QSizePolicy sizePolicy3(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(_slavesList->sizePolicy().hasHeightForWidth());
        _slavesList->setSizePolicy(sizePolicy3);
        _slavesList->setMinimumSize(QSize(221, 0));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Andale Mono"));
        font1.setPointSize(9);
        _slavesList->setFont(font1);
        _slavesList->setAutoFillBackground(false);
        _slavesList->setAutoScroll(true);
        _slavesList->setAutoScrollMargin(20);
        _slavesList->setTextElideMode(Qt::ElideNone);
        _slavesList->setIndentation(20);
        _slavesList->setRootIsDecorated(true);
        _slavesList->setItemsExpandable(true);
        _slavesList->setColumnCount(2);
        domain = new domainListWidget(tabObjectConf);
        domain->setObjectName(QString::fromUtf8("domain"));
        domain->setGeometry(QRect(340, 20, 361, 21));
        QSizePolicy sizePolicy4(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy4.setHorizontalStretch(0);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(domain->sizePolicy().hasHeightForWidth());
        domain->setSizePolicy(sizePolicy4);
        domain->setMinimumSize(QSize(0, 0));
        domain->setMaximumSize(QSize(16777215, 16777215));
        domain->setSizeIncrement(QSize(0, 0));
        domain->setBaseSize(QSize(0, 0));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Andale Mono"));
        font2.setPointSize(8);
        domain->setFont(font2);
        domain->setFrameShadow(QFrame::Raised);
        domain->setLineWidth(0);
        domain->setMidLineWidth(0);
        domain->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        domain->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        domain->setAutoScroll(true);
        domain->setAutoScrollMargin(-1);
        domain->setDragEnabled(false);
        domain->setHorizontalScrollMode(QAbstractItemView::ScrollPerItem);
        domain->setFlow(QListView::LeftToRight);
        domain->setResizeMode(QListView::Fixed);
        domain->setSpacing(-1);
        domain->setUniformItemSizes(false);
        domain->setBatchSize(1);
        domain->setWordWrap(true);
        domainsLabel = new QLabel(tabObjectConf);
        domainsLabel->setObjectName(QString::fromUtf8("domainsLabel"));
        domainsLabel->setGeometry(QRect(490, 0, 67, 17));
        domainsLabel->setAlignment(Qt::AlignCenter);
        line = new QFrame(tabObjectConf);
        line->setObjectName(QString::fromUtf8("line"));
        line->setGeometry(QRect(320, 10, 20, 261));
        line->setFrameShape(QFrame::VLine);
        line->setFrameShadow(QFrame::Sunken);
        domain_2 = new domainListWidget(tabObjectConf);
        domain_2->setObjectName(QString::fromUtf8("domain_2"));
        domain_2->setGeometry(QRect(340, 50, 361, 21));
        sizePolicy4.setHeightForWidth(domain_2->sizePolicy().hasHeightForWidth());
        domain_2->setSizePolicy(sizePolicy4);
        domain_2->setMinimumSize(QSize(0, 0));
        domain_2->setMaximumSize(QSize(16777215, 16777215));
        domain_2->setSizeIncrement(QSize(0, 0));
        domain_2->setBaseSize(QSize(0, 0));
        domain_2->setFont(font2);
        domain_2->setFrameShadow(QFrame::Raised);
        domain_2->setLineWidth(0);
        domain_2->setMidLineWidth(0);
        domain_2->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        domain_2->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        domain_2->setAutoScroll(true);
        domain_2->setAutoScrollMargin(-1);
        domain_2->setDragEnabled(false);
        domain_2->setHorizontalScrollMode(QAbstractItemView::ScrollPerItem);
        domain_2->setFlow(QListView::LeftToRight);
        domain_2->setResizeMode(QListView::Fixed);
        domain_2->setSpacing(-1);
        domain_2->setUniformItemSizes(false);
        domain_2->setBatchSize(1);
        domain_2->setWordWrap(true);
        domain_3 = new domainListWidget(tabObjectConf);
        domain_3->setObjectName(QString::fromUtf8("domain_3"));
        domain_3->setGeometry(QRect(340, 80, 361, 21));
        sizePolicy4.setHeightForWidth(domain_3->sizePolicy().hasHeightForWidth());
        domain_3->setSizePolicy(sizePolicy4);
        domain_3->setMinimumSize(QSize(0, 0));
        domain_3->setMaximumSize(QSize(16777215, 16777215));
        domain_3->setSizeIncrement(QSize(0, 0));
        domain_3->setBaseSize(QSize(0, 0));
        domain_3->setFont(font2);
        domain_3->setFrameShadow(QFrame::Raised);
        domain_3->setLineWidth(0);
        domain_3->setMidLineWidth(0);
        domain_3->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        domain_3->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        domain_3->setAutoScroll(true);
        domain_3->setAutoScrollMargin(-1);
        domain_3->setDragEnabled(false);
        domain_3->setHorizontalScrollMode(QAbstractItemView::ScrollPerItem);
        domain_3->setFlow(QListView::LeftToRight);
        domain_3->setResizeMode(QListView::Fixed);
        domain_3->setSpacing(-1);
        domain_3->setUniformItemSizes(false);
        domain_3->setBatchSize(1);
        domain_3->setWordWrap(true);
        domain_4 = new domainListWidget(tabObjectConf);
        domain_4->setObjectName(QString::fromUtf8("domain_4"));
        domain_4->setGeometry(QRect(340, 110, 361, 21));
        sizePolicy4.setHeightForWidth(domain_4->sizePolicy().hasHeightForWidth());
        domain_4->setSizePolicy(sizePolicy4);
        domain_4->setMinimumSize(QSize(0, 0));
        domain_4->setMaximumSize(QSize(16777215, 16777215));
        domain_4->setSizeIncrement(QSize(0, 0));
        domain_4->setBaseSize(QSize(0, 0));
        domain_4->setFont(font2);
        domain_4->setFrameShadow(QFrame::Raised);
        domain_4->setLineWidth(0);
        domain_4->setMidLineWidth(0);
        domain_4->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        domain_4->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        domain_4->setAutoScroll(true);
        domain_4->setAutoScrollMargin(-1);
        domain_4->setDragEnabled(false);
        domain_4->setHorizontalScrollMode(QAbstractItemView::ScrollPerItem);
        domain_4->setFlow(QListView::LeftToRight);
        domain_4->setResizeMode(QListView::Fixed);
        domain_4->setSpacing(-1);
        domain_4->setUniformItemSizes(false);
        domain_4->setBatchSize(1);
        domain_4->setWordWrap(true);
        domain_5 = new domainListWidget(tabObjectConf);
        domain_5->setObjectName(QString::fromUtf8("domain_5"));
        domain_5->setGeometry(QRect(340, 140, 361, 21));
        sizePolicy4.setHeightForWidth(domain_5->sizePolicy().hasHeightForWidth());
        domain_5->setSizePolicy(sizePolicy4);
        domain_5->setMinimumSize(QSize(0, 0));
        domain_5->setMaximumSize(QSize(16777215, 16777215));
        domain_5->setSizeIncrement(QSize(0, 0));
        domain_5->setBaseSize(QSize(0, 0));
        domain_5->setFont(font2);
        domain_5->setFrameShadow(QFrame::Raised);
        domain_5->setLineWidth(0);
        domain_5->setMidLineWidth(0);
        domain_5->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        domain_5->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        domain_5->setAutoScroll(true);
        domain_5->setAutoScrollMargin(-1);
        domain_5->setDragEnabled(false);
        domain_5->setHorizontalScrollMode(QAbstractItemView::ScrollPerItem);
        domain_5->setFlow(QListView::LeftToRight);
        domain_5->setResizeMode(QListView::Fixed);
        domain_5->setSpacing(-1);
        domain_5->setUniformItemSizes(false);
        domain_5->setBatchSize(1);
        domain_5->setWordWrap(true);
        domain_6 = new domainListWidget(tabObjectConf);
        domain_6->setObjectName(QString::fromUtf8("domain_6"));
        domain_6->setGeometry(QRect(340, 170, 361, 21));
        sizePolicy4.setHeightForWidth(domain_6->sizePolicy().hasHeightForWidth());
        domain_6->setSizePolicy(sizePolicy4);
        domain_6->setMinimumSize(QSize(0, 0));
        domain_6->setMaximumSize(QSize(16777215, 16777215));
        domain_6->setSizeIncrement(QSize(0, 0));
        domain_6->setBaseSize(QSize(0, 0));
        domain_6->setFont(font2);
        domain_6->setFrameShadow(QFrame::Raised);
        domain_6->setLineWidth(0);
        domain_6->setMidLineWidth(0);
        domain_6->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        domain_6->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        domain_6->setAutoScroll(true);
        domain_6->setAutoScrollMargin(-1);
        domain_6->setDragEnabled(false);
        domain_6->setHorizontalScrollMode(QAbstractItemView::ScrollPerItem);
        domain_6->setFlow(QListView::LeftToRight);
        domain_6->setResizeMode(QListView::Fixed);
        domain_6->setSpacing(-1);
        domain_6->setUniformItemSizes(false);
        domain_6->setBatchSize(1);
        domain_6->setWordWrap(true);
        domain_7 = new domainListWidget(tabObjectConf);
        domain_7->setObjectName(QString::fromUtf8("domain_7"));
        domain_7->setGeometry(QRect(340, 200, 361, 21));
        sizePolicy4.setHeightForWidth(domain_7->sizePolicy().hasHeightForWidth());
        domain_7->setSizePolicy(sizePolicy4);
        domain_7->setMinimumSize(QSize(0, 0));
        domain_7->setMaximumSize(QSize(16777215, 16777215));
        domain_7->setSizeIncrement(QSize(0, 0));
        domain_7->setBaseSize(QSize(0, 0));
        domain_7->setFont(font2);
        domain_7->setFrameShadow(QFrame::Raised);
        domain_7->setLineWidth(0);
        domain_7->setMidLineWidth(0);
        domain_7->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        domain_7->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        domain_7->setAutoScroll(true);
        domain_7->setAutoScrollMargin(-1);
        domain_7->setDragEnabled(false);
        domain_7->setHorizontalScrollMode(QAbstractItemView::ScrollPerItem);
        domain_7->setFlow(QListView::LeftToRight);
        domain_7->setResizeMode(QListView::Fixed);
        domain_7->setSpacing(-1);
        domain_7->setUniformItemSizes(false);
        domain_7->setBatchSize(1);
        domain_7->setWordWrap(true);
        codeGenerateButton = new QPushButton(tabObjectConf);
        codeGenerateButton->setObjectName(QString::fromUtf8("codeGenerateButton"));
        codeGenerateButton->setGeometry(QRect(600, 280, 96, 27));
        tabWidget->addTab(tabObjectConf, QString());
        tabOutputConsole = new QWidget();
        tabOutputConsole->setObjectName(QString::fromUtf8("tabOutputConsole"));
        plainTextEdit = new QPlainTextEdit(tabOutputConsole);
        plainTextEdit->setObjectName(QString::fromUtf8("plainTextEdit"));
        plainTextEdit->setGeometry(QRect(10, 10, 691, 271));
        tabWidget->addTab(tabOutputConsole, QString());

        retranslateUi(ProjectDialog);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(ProjectDialog);
    } // setupUi

    void retranslateUi(QDialog *ProjectDialog)
    {
        ProjectDialog->setWindowTitle(QApplication::translate("ProjectDialog", "Objects Editor", 0));
#ifndef QT_NO_TOOLTIP
        ProjectDialog->setToolTip(QString());
#endif // QT_NO_TOOLTIP
        domainsLabel->setText(QApplication::translate("ProjectDialog", "<html><head/><body><p><span style=\" font-family:'Sans Serif'; font-weight:600;\">"
                                                                       "Domains</span></p></body></html>",0));
        codeGenerateButton->setText(QApplication::translate("ProjectDialog", "Generate", 0));
        tabWidget->setTabText(tabWidget->indexOf(tabObjectConf), QApplication::translate("ProjectDialog", "domains Manager", 0));
        tabWidget->setTabText(tabWidget->indexOf(tabOutputConsole), QApplication::translate("ProjectDialog", "Output Console", 0));
    } // retranslateUi

};

namespace Ui {
    class ProjectDialog: public Ui_ProjectDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DOMAINDIALOG_H
