#ifndef XMLSTREAMREADER_H
#define XMLSTREAMREADER_H

#include <QXmlStreamReader>
#include "slavewidget.h"
#include "pdoEntry.h"
#include <list>
using namespace std;

class QTreeWidget;
class QTreeWidgetItem;

static inline long toHex(string hexStr)
{
    char *endptr;
    const char *str = hexStr.c_str();
    return strtol(str, &endptr, 16);
}


class XmlStreamReader
{
public:

    XmlStreamReader(slavesTree *tree);

    bool readFile(const QString &fileName);
    list <pdoEntry> entries;
    void update_pdoEntryList(
            QTreeWidgetItem *itemIndex,
            QTreeWidgetItem *itemSubIndex,
            QString& Index,
            QString &subIndex,
            QString& pdo,
            QString &sm,
            QString &slave);


private:

    QTreeWidgetItem* readVendorIdElement(QString &vid);
    void readEcatInfoListElement();
    void readEcatInfoElement(int i);
    QTreeWidgetItem* readDescriptionsElement(int i);
    void readSmElement(QTreeWidgetItem *parent,int slavePos, int syncPos);
    QTreeWidgetItem* readDevicesElement(int i);
    QTreeWidgetItem* readDeviceElement(int i);
    void readTypeElement(QTreeWidgetItem *parent,int slave_id);
    void readPdoElement(int slave_pos,QString name);
    void readEntryElement(QTreeWidgetItem *parent,MPdo *mpdo);
     QTreeWidgetItem*  readElement(QTreeWidgetItem *parent,QString element,MPdo *mpdo);
    void skipUnknownElement();
    void readEntryElementIndex(QTreeWidgetItem *parent);


private:

    slavesTree *treeWidget;
    QXmlStreamReader reader;
};

#endif
