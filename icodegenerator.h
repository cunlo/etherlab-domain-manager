#ifndef ICODEGENERATOR_H
#define ICODEGENERATOR_H

#include <QtGui>

class ICodeGenerator
{
public:

    virtual void startMain(QString &main, QStringList& domains,QStringList& slaves) = 0;
    virtual void addPlainText(QString &text) = 0;

    virtual void configureSlave(
            QString &Slave,
            QString &alias,
            QString &pos,
            QString &vendorid,
            QString &productCode) = 0;

    virtual void requestMaster() = 0;
    virtual void createDomain(QString &domain) = 0 ;
    virtual void domainData(QString &text) = 0;
    virtual void endMain(QString &text) = 0;
    virtual void activateMaster() = 0;
    virtual void addPdoEntry(
            QString &Slave,
            QString &Index,
            QString &subIndex,
            QString &domain) = 0;
    virtual void createStructs() = 0;
    virtual void configurePDos(QString &slavePos) = 0;
    virtual void createIncludes() = 0;

};

#endif // ICODEGENERATOR_H
