#include <QtGui>
#include <iostream>

#include "MainWindow.h"

int main(int argc, char *argv[])
{
    int ret = 0;

    QApplication app(argc, argv);
    QStringList args = QApplication::arguments();
    MainWindow Main;

    if (args.count() < 2) {
        std::cerr << "Usage: " << args[0].toStdString() <<  " file.xml..." <<
                     std::endl;
        return 1;
    }

    try {
            ProjectDialog* dialog = new ProjectDialog();
            dialog->openXMLFile(QString(args[1]));
            dialog->setMainWindow(&Main);
            Main.CreateActions();
            Main.createMenus();
            Main.createToolBars();
            Main.setCentralWidget(dialog);
            int height = 450;
            int width  = 800;
            Main.setGeometry(-1,-1, width, height);
            Main.show();
            ret = app.exec();
    }catch (const char *str) {
        std::cout << str << std::endl;
    }
    return ret;
}
