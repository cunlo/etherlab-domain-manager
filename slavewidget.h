#ifndef MYTREEWIDGET_H
#define MYTREEWIDGET_H

//#include <QtGui>
#include <QTreeWidgetItem>

class ProjectDialog;
class domainListWidget;

class pdoItem : public QTreeWidgetItem
{

 QString _pdoEntryPath;

public:

 pdoItem(QTreeWidgetItem *parent):QTreeWidgetItem(parent){}

 QString getPdoEntryPath()
 {
    return _pdoEntryPath;
 }

 void setPdoEntryPath(QString pdoEntryPath)
 {
    _pdoEntryPath = pdoEntryPath;
 }
};

class slavesTree : public QTreeWidget
{
	Q_OBJECT

public:
    slavesTree(QWidget *parent = 0);

	void mouseMoveEvent(QMouseEvent *event);
    virtual bool dropMimeData(QTreeWidgetItem *parent, int index, \
                                const QMimeData *data, Qt::DropAction action);
    virtual bool dropMimeData(domainListWidget *parent, int index,\
                                const QMimeData *data, Qt::DropAction action);
    QStringList mimeTypes() const;
	Qt::DropActions supportedDropActions () const;
    void dragMoveEvent(QDragMoveEvent *event);
    void dragEnterEvent(QDragEnterEvent *event);
    static bool getEntryFullPath(QTreeWidgetItem *Entry, QString &fullPath);
    static QTreeWidgetItem* getChild(QTreeWidgetItem *root,QString variable,QString& value);
    void setProjectDialog(ProjectDialog *d){_dialog = d;}
    ProjectDialog *Dialog() { return _dialog;}
private:
     void performDrag();
     ProjectDialog *_dialog;
};


#endif
