#ifndef PDODOMAINITEM_H
#define PDODOMAINITEM_H

#include <QListWidgetItem>

class pdoDomainItem : public QListWidgetItem
{
    QString _parentPath;
    QString _Index;
    QString _subIndex;
    QString _pdoIndex;
    QString _SM;
    QString _slave;

public:

     pdoDomainItem(){}

     QString getParentPath(){
        return _parentPath;
     }
     void getElements(QString &index,QString &subIndex,QString &pdoIndex,QString &sm,QString &slave);
     void setEntry(QString& entry);
     void setParentPath(QString& fpath);
};

#endif // PDODOMAINITEM_H
