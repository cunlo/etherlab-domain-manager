#ifndef PDOENTRY_H
#define PDOENTRY_H

class QTreeWidgetItem;

class pdoEntry {
public:
    pdoEntry(QString& index,
             QString& subindex,
             QString& pdo,
             QString& sm,
             QString& slave){
        _Index = index;
        _subIndex = subindex;
        _pdo = pdo;
        _sm = sm;
        _slave = slave;
    }

    QString _Index;
    QString _subIndex;
    QString _pdo;
    QString _sm;
    QString _slave;
    QTreeWidgetItem *_itemIndex;
    QTreeWidgetItem *_itemSubIndex;
    QTreeWidgetItem *_itemEntry;
};

#endif // PDOENTRY_H
