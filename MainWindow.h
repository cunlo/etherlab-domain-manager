#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QToolBar>
#include "domaindialog.h"

class MainWindow : public QMainWindow{

    Q_OBJECT

    QToolBar *fileToolBar;
    QToolBar *domainToolBar;
    QToolBar* saveToolBar;

public:

    MainWindow() {}
    virtual void CreateActions();
    virtual void createMenus();
    virtual void createToolBars();
    virtual void CreateStructs();

private:
    QString curFile;
    QAction* newDomainAction;
    QAction* openFileAction;
    QAction* saveAction;
    QMenu* fileMenu;

    QString strippedName(const QString &fullFileName);
    bool saveFile(const QString &fileName);
    bool saveAs();
    void setCurrentFile(const QString &fileName);

private slots:
    void newDomain();
    void openFile();
    bool saveFile();
};

#endif // MAINWINDOW_H
