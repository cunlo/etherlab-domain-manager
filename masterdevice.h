#ifndef MASTERDEVICE_H
#define MASTERDEVICE_H

#include "ioctl.h"
#include <list>
#include <string>


struct MPdoEntry{
    ec_ioctl_slave_sync_pdo_entry_t entry;
};

struct MPdo {
   ec_ioctl_slave_sync_pdo_t pdo;
   std::list<MPdoEntry> entries;
};

struct MSyncManager {
    ec_ioctl_slave_sync_t sync;
    std::list<MPdo> pdos;
};

struct MSlave {
    ec_ioctl_slave_t slave;
    std::list<MSyncManager> syncs;
};

class ProjectDialog;

class MasterDevice
{
    std::list<MSlave> slaves;
    ProjectDialog *_dialog;

public:

    MasterDevice(ProjectDialog *d);
    ec_ioctl_slave_t* addSlave(int &pos);
    MPdo *addPdo(int slavePos,int syncPos);
    void addSync( int slave_pos, int syncPos);
    void addEntry(MPdo *mpdo,
                  int index,
                  int subIndex,
                  int bitLen,
                  std::string entryName);

    void selectedSlaves(std::list<MSlave> &);
    std::string getIndex() { return std::string("0");}

    ec_ioctl_slave_t* getSlave(int pos);

    void getSync(ec_ioctl_slave_sync_t *sync, int slave_pos, unsigned int syncPos);
    void getPdo( ec_ioctl_slave_sync_pdo_t *pdo, int slave_pos, int syncPos, int pdoPos);
    void getPdoEntry(ec_ioctl_slave_sync_pdo_entry_t* entry, int slave_pos,  int syncPos, int pdoPos, int entryPos);

    MSlave* Slave(int slavePos);
    MSyncManager* Sync(int slavePos, unsigned int syncPos);
    MPdo* mPdo(int slavePos, int syncPos, int pdoPos);
};

#endif // MASTERDEVICE_H
