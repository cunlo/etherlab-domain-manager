#ifndef CODECGENERATOR_H
#define CODECGENERATOR_H

#include <QtGui>

#include "masterdevice.h"

class ProjectDialog;

class codeCgenerator : public ICodeGenerator
{
    ProjectDialog *_dialog;

public:

    codeCgenerator(ProjectDialog *d) {_dialog = d;}

    void addPlainText(QString &text);

    void configureSlave(
            QString &Slave,
            QString &alias,
            QString &pos,
            QString &vendorid,
            QString &productCode);

    void requestMaster();
    void createDomain(QString &domain);
    void domainData(QString &text);
    void endMain(QString &text);
    void startMain(QString &main, QStringList& domains,QStringList& slaves);
    void activateMaster();
    void configurePDos(QString &slavePos);
    void addPdoEntry(
            QString &Slave,
            QString &Index,
            QString &subIndex,
            QString &domain);

    void createStructs();
    void generateSlaveCStruct(
            MasterDevice *m,
            const ec_ioctl_slave_t &slave);
    void createIncludes();
};

#endif // CODECGENERATOR_H
