#include <QFileDialog>
#include <QStatusBar>
#include <QMenuBar>

#include "MainWindow.h"

void MainWindow::CreateActions()
{
    newDomainAction = new QAction(tr("&New"), this);
    newDomainAction->setIcon(QIcon(":images/new.png"));
    newDomainAction->setShortcut(QKeySequence::New);
    newDomainAction->setStatusTip(tr("Create New Domain"));
    connect(newDomainAction,SIGNAL(triggered()),this,SLOT(newDomain()));

    openFileAction = new QAction(tr("&Open"),this);
    openFileAction->setIcon(QIcon(":images/open.png"));
    openFileAction->setShortcut(QKeySequence::Open);
    openFileAction->setStatusTip(tr("open etherlab xml file"));
    connect(openFileAction,SIGNAL(triggered()),this,SLOT(openFile()));

    saveAction = new QAction(tr("&Save"), this);
    saveAction->setIcon(QIcon(":/images/save.png"));
    saveAction->setShortcut(QKeySequence::Save);
    saveAction->setStatusTip(tr("save the generated C code disk"));
    connect(saveAction, SIGNAL(triggered()), this, SLOT(saveFile()) );
}


bool MainWindow::saveFile()
{
    if (curFile.isEmpty()) {
        return saveAs();
    } else {
        return saveFile(curFile);
    }
}

void MainWindow::CreateStructs()
{
      ProjectDialog * _dialog = (ProjectDialog *) centralWidget();
      _dialog->codeGenerator->createStructs();
}

bool MainWindow::saveAs()
{
    QString fileName = QFileDialog::getSaveFileName(this,
                               tr("Save Generated c"), ".",
                               tr("c source files (*.c)"));
    if (fileName.isEmpty())
        return false;

    return saveFile(fileName);
}

bool MainWindow::saveFile(const QString &fileName)
{
    ProjectDialog * _dialog = (ProjectDialog *) centralWidget();
    if (_dialog->writeCFile(fileName)) {
        statusBar()->showMessage(tr("Saving canceled"), 2000);
        return false;
    }
    setCurrentFile(fileName);
    statusBar()->showMessage(tr("File saved"), 2000);
    return true;
}

void MainWindow::setCurrentFile(const QString &fileName)
{
    curFile = fileName;
    setWindowModified(false);

    QString shownName = tr("Untitled");
    if (!curFile.isEmpty()) {
        shownName = strippedName(curFile);
    }
    setWindowTitle(tr("%1[*] - %2").arg(shownName)
                                   .arg(tr("elab")));
}

QString MainWindow::strippedName(const QString &fullFileName)
{
    return QFileInfo(fullFileName).fileName();
}


void MainWindow::openFile()
{
      statusBar()->showMessage(tr("xml File open"), 2000);
      QString fileName = QFileDialog::getOpenFileName(this,
                                 tr("Open an ESI file"), ".",
                                 tr("XML files (*.xml)"));
      setCurrentFile(fileName);
      return;


}

void MainWindow::newDomain()
{
    statusBar()->showMessage(tr("new domain created"), 2000);
    ProjectDialog * _dialog = (ProjectDialog *) centralWidget();
    _dialog->newDomain();
}

void MainWindow::createMenus()
{
    fileMenu = menuBar()->addMenu(tr("&File"));
    fileMenu->addAction(saveAction);
    fileMenu->addAction(newDomainAction);
    fileMenu->addAction(openFileAction);
    fileMenu->addSeparator();

}

void MainWindow::createToolBars()
{
    fileToolBar = addToolBar(tr("&File"));
    fileToolBar->addAction(openFileAction);
    openFileAction->setToolTip(tr("opens an XML etherlab file"));

    saveToolBar = addToolBar(tr("&Save"));
    saveToolBar->addAction(saveAction);
    saveToolBar->setToolTip(tr("save generated c"));

    domainToolBar= addToolBar(tr("&New"));
    domainToolBar->addAction(newDomainAction);
    newDomainAction->setToolTip(tr("Create a new domain"));
}
