#include <QtGui>
#include <sstream>
#include <iomanip>
#include <string.h>
#include <iostream>

using namespace std;

#include "domaindialog.h"
#include "codecgenerator.h"

#define BLANKS "    "

void codeCgenerator::createIncludes()
{
    stringstream output;

    output << "#include <errno.h>" << std::endl
            << "#include <signal.h>" << std::endl
            << "#include <stdio.h>" << std::endl
            << "#include <string.h>" << std::endl
            << "#include <sys/resource.h>" << std::endl
            << "#include <sys/time.h>" << std::endl
            << "#include <sys/types.h>" << std::endl
            << "#include <unistd.h>" << std::endl
            << "#include <time.h>" << std::endl
            << "#include <sys/mman.h>" << std::endl
            << "#include <malloc.h>" << std::endl
            << "#include \"ecrt.h\" " << std::endl;

    QString Code(output.str().c_str());
    _dialog->appendText(Code);

}

void codeCgenerator::startMain(QString &main, QStringList& domains,QStringList& slaves)
{
    stringstream output;

    output << "\n" << main.toStdString()
           << "{" << "\n"
           << BLANKS << "int ret;\n"
           << BLANKS << "int bitposition;" << "\n";

    output << BLANKS << "ec_master_t *master;" << "\n";

    foreach(const QString &str, domains){
        output << "\n" << BLANKS << "ec_domain_t *" <<
               str.toStdString() << ";\n" <<
               BLANKS <<  "uint8_t *"
               << str.toStdString() << "_pd;" << std::endl;
    }

    foreach(const QString &str, slaves){
        output << "\n" << BLANKS << "ec_slave_config_t *"
        << str.toStdString() << ";\n" ;
    }

    QString Code(output.str().c_str());
    _dialog->appendText(Code);
}

void codeCgenerator::endMain(QString &text)
{
    _dialog->appendText(text);
}

void codeCgenerator::configurePDos(QString &slavePos)
{
    stringstream output;

    output  << BLANKS
            << "if (ecrt_slave_config_pdos( "
            << "Device" << slavePos.toStdString()
            << " , EC_END,"
            <<  "slave_" << slavePos.toStdString()
            << "_syncs"
            << ")){\n"
            << BLANKS  BLANKS
            << "return -1;" << "\n"
            << BLANKS << "}\n";

    QString Code(output.str().c_str());
    _dialog->appendText(Code);
}


void codeCgenerator::activateMaster()
{
    stringstream output;

    output << BLANKS << "if ( ecrt_master_activate(master) )" <<
               BLANKS BLANKS << "return -1;";

    QString Code(output.str().c_str());
    _dialog->appendText(Code);
}

void codeCgenerator::addPdoEntry(
        QString &Slave,
        QString &Index,
        QString &subIndex,
        QString &domain)
{
    stringstream output;

    output << BLANKS  <<   "ret = ecrt_slave_config_reg_pdo_entry("
            << Slave.toStdString()
            << ","
            << Index.toStdString()
            << ","
            << subIndex.toStdString()
            << ","
            << domain.toStdString()
            << ", &bitposition);\n"
            << BLANKS << "if (ret) return -1;" << "\n";

    QString Code(output.str().c_str());
    _dialog->appendText(Code);
}

void codeCgenerator::addPlainText(QString &text)
{
    _dialog->appendText(text);
}

void codeCgenerator::configureSlave(
        QString &Slave,
        QString &alias,
        QString &pos,
        QString &vendorid,
        QString &productCode)
{
    stringstream output;

    output << BLANKS <<  Slave.toStdString() <<
            " = ecrt_master_slave_config(master ,"
            << alias.toStdString() << ","
            << pos.toStdString() << ","
            <<  vendorid.toStdString() << ","
            <<  productCode.toStdString()  << ");\n"
            << BLANKS << "if ( "  << Slave.toStdString()
            << "== NULL ) return -1;\n\n";

    QString Code(output.str().c_str());
    _dialog->appendText(Code);
}

void codeCgenerator::requestMaster()
{
    stringstream output;

    output  << BLANKS << "master = ecrt_request_master(0);\n"
            << BLANKS << "if (!master) return -1;\n";

    QString Code(output.str().c_str());
    _dialog->appendText(Code);
}

void codeCgenerator::createDomain(QString &domain)
{
    stringstream output;

    output << BLANKS << domain.toStdString()
           <<  " = ecrt_master_create_domain(master);\n"
           << BLANKS << "if ( !"
           << domain.toStdString()
           << " ) return -1;\n";

    QString Code(output.str().c_str());
    _dialog->appendText(Code);
}

void codeCgenerator::domainData(QString &domain)
{
        stringstream output;

        output << BLANKS <<  domain.toStdString() << "_pd"
                << " = ecrt_domain_data( " << domain.toStdString()
                << " );" << "\n"
                << BLANKS << "if ( " << domain.toStdString() << "_pd"
                << " == NULL ) \n" << BLANKS BLANKS << "return -1;";

      QString Code(output.str().c_str());
      _dialog->appendText(Code);
}
